package com.example.inote.datasource.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.inote.utils.constant.Constant;

@Entity(tableName = Constant.TABLE_NAME_CHECKBOX)
public class CheckboxModel {
    @PrimaryKey(autoGenerate = true)
    private int id;

    private String textCheck;
    private String isCheck;
    private int idUser;

    public CheckboxModel(String textCheck, String isCheck, int idUser) {
        this.textCheck = textCheck;
        this.isCheck = isCheck;
        this.idUser = idUser;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTextCheck() {
        return textCheck;
    }

    public void setTextCheck(String textCheck) {
        this.textCheck = textCheck;
    }

    public String getIsCheck() {
        return isCheck;
    }

    public void setIsCheck(String isCheck) {
        this.isCheck = isCheck;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }
}
