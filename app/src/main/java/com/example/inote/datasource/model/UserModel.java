package com.example.inote.datasource.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.inote.utils.constant.Constant;

import java.io.Serializable;

@Entity(tableName = Constant.TABLE_NAME_USER)
public class  UserModel implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String title;
    private String content;
    private String date;
    private String isCheck;

    public UserModel(String title, String content, String date , String isCheck) {
        this.title = title;
        this.content = content;
        this.date = date;
        this.isCheck = isCheck;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getIsCheck() {
        return isCheck;
    }

    public void setIsCheck(String isCheck) {
        this.isCheck = isCheck;
    }
}
