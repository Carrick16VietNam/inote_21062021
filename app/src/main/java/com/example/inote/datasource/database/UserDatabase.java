package com.example.inote.datasource.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.inote.datasource.model.CheckboxModel;
import com.example.inote.datasource.model.PaintModel;
import com.example.inote.datasource.model.PictureModel;
import com.example.inote.datasource.model.UserModel;
import com.example.inote.utils.constant.Constant;

@Database(entities = {UserModel.class, CheckboxModel.class, PictureModel.class, PaintModel.class},version = 1)
public abstract class UserDatabase extends RoomDatabase {
    private static UserDatabase instance;

    public static UserDatabase getInstance(Context context) {
        if(instance==null){
            instance = Room.databaseBuilder(context,UserDatabase.class, Constant.NAME_DATABASE)
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    }

    public abstract UserDao userDao();
}
