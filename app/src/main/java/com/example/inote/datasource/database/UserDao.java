package com.example.inote.datasource.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.inote.datasource.model.CheckboxModel;
import com.example.inote.datasource.model.PaintModel;
import com.example.inote.datasource.model.PictureModel;
import com.example.inote.datasource.model.UserModel;
import com.example.inote.utils.constant.Constant;

import java.util.List;

@Dao
public interface UserDao {
    @Insert
    public void insertUser(UserModel userModel);

    @Query("Select *From "+ Constant.TABLE_NAME_USER)
    public List<UserModel> listUser();

    @Delete
    public void deleteUser(UserModel userModel);

    @Update
    public void updateUser(UserModel userModel);

    @Query("Select *From "+Constant.TABLE_NAME_USER+" Where isCheck = :str")
    public List<UserModel> listPinnedOrNotes(String str);

    @Query("Select *From "+Constant.TABLE_NAME_USER+" Where (title Like '%'||:str||'%' or content Like '%'||:str||'%') and isCheck = :check")
    public List<UserModel> searchUser(String str, String check);

    @Query("Select *From "+Constant.TABLE_NAME_USER+" Where isCheck = :str ORDER BY title ASC")
    public List<UserModel> listUserAZ(String str);

    @Query("Select *From "+Constant.TABLE_NAME_USER+" Where isCheck = :str ORDER BY title DESC")
    public List<UserModel> listUserZA(String str);

    @Query("Select *From "+Constant.TABLE_NAME_CHECKBOX+" Where idUser = :id ")
    public List<CheckboxModel> listCheckBoxModel(int id);

    @Insert
    public void insertCheckboxModel(CheckboxModel checkboxModel);

    @Delete
    public void deleteListCheckBoxModel(List<CheckboxModel> list);

    @Update
    public void updateCheckboxModel(CheckboxModel checkboxModel);

    @Delete
    public  void deleteCheckboxModel(CheckboxModel checkboxModel);

    @Query("Select *From "+Constant.TABLE_NAME_PICTURE+" Where idUser = :id")
    public List<PictureModel> listPictureModel(int id);

    @Delete
    public void deleteListPictureModel(List<PictureModel> listPictureModel);

    @Insert
    public void insertPictureModel(PictureModel pictureModel);

    @Delete
    public void deletePictureModel(PictureModel pictureModel);

    @Insert
    public void insertPaintModel(PaintModel paintModel);

    @Query("Select *From "+Constant.TABLE_NAME_PAINT+" Where idUser = :id")
    public List<PaintModel> mListPaintModel(int id);

    @Delete
    public void deleteListPaintModel(List<PaintModel> listPaintModel);

    @Delete
    public void deletePaintModel(PaintModel paintModel);


}
