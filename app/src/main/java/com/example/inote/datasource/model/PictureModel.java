package com.example.inote.datasource.model;

import android.net.Uri;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.inote.utils.constant.Constant;

@Entity(tableName = Constant.TABLE_NAME_PICTURE)
public class PictureModel {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String uri;
    private int idUser;

    public PictureModel(String uri, int idUser) {
        this.uri = uri;
        this.idUser = idUser;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }
}
