package com.example.inote.datasource.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.inote.utils.constant.Constant;

@Entity(tableName = Constant.TABLE_NAME_PAINT)
public class PaintModel {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String path;
    private int idUser;

    public PaintModel(String path, int idUser) {
        this.path = path;
        this.idUser = idUser;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }
}
