package com.example.inote.utils.app;

import android.app.Application;

import com.example.inote.ui.act.splash.SplashAct;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class App extends Application {
    private static App instance;
    private boolean isCheckPinnedorAllNotesMenu = true;
    private int markEachScreen = 2;


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

    }


    public static App getInstance() {
        return instance;
    }

    public static void setInstance(App instance) {
        App.instance = instance;
    }

    public boolean isCheckPinnedorAllNotesMenu() {
        return isCheckPinnedorAllNotesMenu;
    }

    public void setCheckPinnedorAllNotesMenu(boolean checkPinnedorAllNotesMenu) {
        isCheckPinnedorAllNotesMenu = checkPinnedorAllNotesMenu;
    }

    public int getMarkEachScreen() {
        return markEachScreen;
    }

    public void setMarkEachScreen(int markEachScreen) {
        this.markEachScreen = markEachScreen;
    }
}
