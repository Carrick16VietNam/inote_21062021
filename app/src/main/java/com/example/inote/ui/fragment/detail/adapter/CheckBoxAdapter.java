package com.example.inote.ui.fragment.detail.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.inote.R;
import com.example.inote.datasource.model.CheckboxModel;
import com.example.inote.ui.callback.OnActionCallBack;

import java.util.List;

public class CheckBoxAdapter extends RecyclerView.Adapter<CheckBoxAdapter.ViewHolder> {
    public static final String KEY_CHECKBOXADAPTER_TO_DETAIL_ONECLICK = "KEY_CHECKBOXADAPTER_TO_DETAIL_ONECLICK";
    public static final String KEY_MENU_CHECKBOXADAPTER = "KEY_MENU_CHECKBOXADAPTER";
    public static final String KEY_DELETE_CHECKBOXADAPTER = "KEY_DELETE_CHECKBOXADAPTER";
    public static final String KEY_REPAIR_CHECKBOXADAPTER = "KEY_REPAIR_CHECKBOXADAPTER";
    private Context mContext;
    private List<CheckboxModel> listCheckboxModel;
    private OnActionCallBack callBack;

    public CheckBoxAdapter(Context mContext, List<CheckboxModel> listCheckboxModel) {
        this.mContext = mContext;
        this.listCheckboxModel = listCheckboxModel;
    }

    public void setNewData(List<CheckboxModel> list) {
        listCheckboxModel = list;
        notifyDataSetChanged();
    }

    public void setCallBack(OnActionCallBack callBack) {
        this.callBack = callBack;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_item_checkbox_detail, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CheckboxModel checkboxModel = listCheckboxModel.get(position);
        holder.tvTextCheckboxDetail.setText(checkboxModel.getTextCheck());

        if (Boolean.parseBoolean(checkboxModel.getIsCheck())) {
            holder.imvCheckTrueFalseDetail.setImageResource(R.drawable.ic_checkbox_true_detail);
//            holder.tvTextCheckboxDetail.setBackgroundResource(R.drawable.custom_strikethrough_textview_checkbox_detail);
        } else {
            holder.imvCheckTrueFalseDetail.setImageResource(R.drawable.ic_checkbox_false_detail);
//            holder.tvTextCheckboxDetail.setBackgroundResource(R.drawable.custom_background_default_checkbox_detail);
        }

        holder.constrainLayoutItemCheckboxDetail.setBackgroundResource(R.color.gray);

        holder.imvMenuCheckBoxDetail.setVisibility(View.GONE);
        holder.imvDeleteCheckboxDetail.setVisibility(View.GONE);
        holder.imvRepairCheckboxDetail.setVisibility(View.GONE);

        holder.constrainLayoutItemCheckboxDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callBack.onCallBackCheckbox(KEY_CHECKBOXADAPTER_TO_DETAIL_ONECLICK, checkboxModel);

                holder.constrainLayoutItemCheckboxDetail.setBackgroundResource(R.color.gray);

                holder.imvMenuCheckBoxDetail.setVisibility(View.GONE);
                holder.imvDeleteCheckboxDetail.setVisibility(View.GONE);
                holder.imvRepairCheckboxDetail.setVisibility(View.GONE);
            }
        });

        holder.constrainLayoutItemCheckboxDetail.setOnLongClickListener(new View.OnLongClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public boolean onLongClick(View view) {
                holder.constrainLayoutItemCheckboxDetail.setBackgroundResource(R.color.gray_background_edittext);

                holder.imvMenuCheckBoxDetail.setVisibility(View.VISIBLE);
                holder.imvDeleteCheckboxDetail.setVisibility(View.VISIBLE);
                holder.imvRepairCheckboxDetail.setVisibility(View.VISIBLE);

                return true;
            }
        });


        holder.imvDeleteCheckboxDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.onCallBackCheckbox(KEY_DELETE_CHECKBOXADAPTER, checkboxModel);
            }
        });

        holder.imvRepairCheckboxDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.onCallBackCheckbox(KEY_REPAIR_CHECKBOXADAPTER, checkboxModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listCheckboxModel.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imvCheckTrueFalseDetail;
        TextView tvTextCheckboxDetail;
        ConstraintLayout constrainLayoutItemCheckboxDetail;
        ImageView imvMenuCheckBoxDetail, imvDeleteCheckboxDetail, imvRepairCheckboxDetail;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imvCheckTrueFalseDetail = itemView.findViewById(R.id.imv_check_true_false_detail);
            tvTextCheckboxDetail = itemView.findViewById(R.id.tv_text_checkbox_detail);
            constrainLayoutItemCheckboxDetail = itemView.findViewById(R.id.constrain_layout_item_checkbox_detail);
            imvMenuCheckBoxDetail = itemView.findViewById(R.id.imv_menu_checkbox_detail);
            imvDeleteCheckboxDetail = itemView.findViewById(R.id.imv_delete_checkbox_detail);
            imvRepairCheckboxDetail = itemView.findViewById(R.id.imv_repair_checkbox_detail);
        }
    }
}
