package com.example.inote.ui.fragment.detail;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.inote.R;
import com.example.inote.base.BaseFragment;
import com.example.inote.datasource.model.CheckboxModel;
import com.example.inote.datasource.model.PaintModel;
import com.example.inote.datasource.model.PictureModel;
import com.example.inote.datasource.model.UserModel;
import com.example.inote.ui.act.main.MainAct;
import com.example.inote.ui.callback.OnActionCallBack;
import com.example.inote.ui.callback.OnActionCallbackFragment;
import com.example.inote.ui.fragment.detail.adapter.CheckBoxAdapter;
import com.example.inote.ui.fragment.detail.adapter.PaintAdapter;
import com.example.inote.ui.fragment.detail.adapter.PictureAdapter;
import com.example.inote.utils.app.App;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import gun0912.tedbottompicker.TedBottomPicker;

public class DetailFragment extends BaseFragment<DetailViewModel> implements OnActionCallBack {
    public static final String KEY_DRAW_PAINT_FRAGMENT = "KEY_DRAW_PAINT_FRAGMENT";
    private TextView tvTimeDetail;
    private EditText edtTitle , edtContent;
    private ImageView imvPinDetail;
    private UserModel userModel = null;
    private List<CheckboxModel> listCheckboxModel;
    private List<PictureModel> listPictureModel;
    private List<PaintModel> listPaintModel;
    private RecyclerView rcvCheckboxDetail , rcvPictureDetail , rcvPaintDetail;
    private CheckBoxAdapter checkBoxAdapter;
    private PictureAdapter pictureAdapter;
    private PaintAdapter paintAdapter;
    private OnActionCallbackFragment callback;
    private FirebaseAnalytics mFirebaseAnalytics;

    public void setCallback(OnActionCallbackFragment callback) {
        this.callback = callback;
    }

    @Override
    public void onResume() {
        super.onResume();
        Window window = getActivity().getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getActivity().getResources().getColor(R.color.gray));
    }

    @Override
    protected Class<DetailViewModel> getClassModel() {
        return DetailViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frag_detail;
    }

    @SuppressLint("MissingPermission")
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void initViews() {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());

        // Mark each screen
        App.getInstance().setMarkEachScreen(3);

        mModel.setContext(getContext());

        // Hiển thị thời gian của đki của user
        tvTimeDetail = findViewById(R.id.tv_time_detail);
        // Nhập title
        edtTitle = findViewById(R.id.edt_title_detail);
        // Nhập content
        edtContent = findViewById(R.id.edt_content_detail);
        // Dùng để thay đổi pinned or notes của user
        imvPinDetail = findViewById(R.id.imv_pin_detail);

        // các listCheckboxModel , listPictureModel , listPaintModel để hiển thị các ảnh , checkbox của user
        listCheckboxModel = new ArrayList<>();
        listPictureModel = new ArrayList<>();
        listPaintModel = new ArrayList<>();
        rcvCheckboxDetail = findViewById(R.id.rcv_checkbox_detail);
        rcvPictureDetail = findViewById(R.id.rcv_image_detail);
        rcvPaintDetail = findViewById(R.id.rcv_paint_detail);

        // Xử lý các sự kiện
        findViewById(R.id.imv_back_detail,this);
        findViewById(R.id.constrain_layout_back_detail,this);
        findViewById(R.id.constraint_layout_done_detail,this);
        findViewById(R.id.constraint_layout_pin_detail,this);
        findViewById(R.id.constraint_layout_checkbox_detail,this);
        findViewById(R.id.constraint_layout_camera_detail,this);
        findViewById(R.id.constraint_layout_paint_detail,this);

        // Lấy ra các thứ khi mới đầu vào DetailFragment
        getFirstValue();


    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getFirstValue() {
        if(MainAct.KEY_BUNDLE.equals(MainAct.KEYBUNDLE_PINNEDADAPTER_DETAIL)) {
            // Khi mà ấn vào phần tử của PinnedAdapter thì nó sẽ truyền sang phần tử userModel đố sang đây
            Bundle bundle = getArguments();
            userModel = (UserModel) bundle.getSerializable(MainAct.KEYBUNDLE_PINNEDADAPTER_DETAIL);
        }else if(MainAct.KEY_BUNDLE.equals(MainAct.KEYBUNDLE_HOMEFRAGMENT_DETAILFRAGMENT)){
            // Khi ấn thêm user từ nút create thì sẽ gán userModel sẽ là phần tử cuối cùng trong database
            Bundle bundle = getArguments();
            userModel = (UserModel) bundle.getSerializable(MainAct.KEYBUNDLE_HOMEFRAGMENT_DETAILFRAGMENT);
            List<UserModel> list = mModel.getListUser();
            int index = list.size()-1;
            userModel = list.get(index);
        }else if(MainAct.KEY_BUNDLE.equals(MainAct.KEYBUNDLE_NOTESADAPTER_DETAIL)){
            // Khi mà ấn vào phần tử của NotesAdapter thì nó sẽ truyền sang phần tử userModel đố sang đây
            Bundle bundle = getArguments();
            userModel = (UserModel) bundle.getSerializable(MainAct.KEYBUNDLE_NOTESADAPTER_DETAIL);
        }

        // thiết lập các giá trị title , content , time , imvPin cho userModel
        edtTitle.setText(userModel.getTitle());
        edtContent.setText(userModel.getContent());
        tvTimeDetail.setText(userModel.getDate());
        if (Boolean.parseBoolean(userModel.getIsCheck()) == false ) {
            imvPinDetail.setImageResource(R.drawable.ic_pin_detail);
        } else {
            imvPinDetail.setImageResource(R.drawable.ic_unpin_detail);
        }
        // Khi oncreate trong pinned
        if(App.getInstance().isCheckPinnedorAllNotesMenu()==false){
            imvPinDetail.setImageResource(R.drawable.ic_pin_detail);
            userModel.setIsCheck("false");
        }


        // dùng để hiển thị list checkbox của userModel + xử lý callback khi ấn vào từng thằng checkbox
        listCheckboxModel = mModel.getListCheckbox(userModel.getId());
        checkBoxAdapter = new CheckBoxAdapter(getContext(),listCheckboxModel);
        checkBoxAdapter.setCallBack(this);
        rcvCheckboxDetail.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false));
        rcvCheckboxDetail.setAdapter(checkBoxAdapter);

        // dùng để hiển thị list picture của userModel + xử lý callback khi ấn vào từng thằng picture
        listPictureModel = mModel.getListPicture(userModel.getId());
        pictureAdapter = new PictureAdapter(getContext(), listPictureModel);
        pictureAdapter.setCallBack(this);
        rcvPictureDetail.setLayoutManager(new GridLayoutManager(getContext(),2));
        rcvPictureDetail.setAdapter(pictureAdapter);

        // dùng để hiển thị list paint của userModel + xử lý callback khi ấn vào từng thằng paint
        listPaintModel = mModel.getListPaint(userModel.getId());
        paintAdapter = new PaintAdapter(getContext(),listPaintModel);
        paintAdapter.setCallBack(this);
        rcvPaintDetail.setLayoutManager(new GridLayoutManager(getContext(),2));
        rcvPaintDetail.setAdapter(paintAdapter);

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onClick(View v) {
        Bundle params = new Bundle();
        if(v.getId()==R.id.imv_back_detail||v.getId()==R.id.constrain_layout_back_detail){
            backFragment();
        }else if(v.getId()==R.id.constraint_layout_done_detail){
            createNotes();
        }else if(v.getId()==R.id.constraint_layout_pin_detail){
            swapNotesOrPinnedOfUser();
        }else if(v.getId()==R.id.constraint_layout_checkbox_detail){
            showDialogCheckboxTextDetail();
        }else if(v.getId()==R.id.constraint_layout_camera_detail){
            //Check event
            params.putString("event_type","click_Icon_picture");
            mFirebaseAnalytics.logEvent("New_Note_Layout",params);

            addPictureDetail();
        }else if(v.getId()==R.id.constraint_layout_paint_detail) {
            //Check event
            params.putString("event_type","click_Icon_pen");
            mFirebaseAnalytics.logEvent("New_Note_Layout",params);

            callback.onCallback(KEY_DRAW_PAINT_FRAGMENT,userModel);
        }

    }

    private void addPictureDetail() {
        // Check permission bằng tedPermission
        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                openPicture();
            }

            @Override
            public void onPermissionDenied(List<String> deniedPermissions) {
                Toast.makeText(getContext(), "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }
        };

        TedPermission.with(getContext())
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("Check Permission")
                .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA)
                .check();
    }

    private void openPicture() {
        // Khi onPermissionGranted() thì sẽ sử dụng TedBottomPicker để thêm ảnh
        TedBottomPicker.OnImageSelectedListener listener = new TedBottomPicker.OnImageSelectedListener() {
            @Override
            public void onImageSelected(Uri uri) {
                String str = String.valueOf(uri);
                Log.i("TAG",str);

                int id = userModel.getId();
                mModel.insertPictureModel(new PictureModel(str,id));

                listPictureModel = mModel.getListPicture(id);
                pictureAdapter.setNewData(listPictureModel);
            }
        };

        TedBottomPicker tedBottomPicker = new TedBottomPicker.Builder(getContext())
                .setOnImageSelectedListener(listener)
                .create();
        tedBottomPicker.show(getFragmentManager());
    }

    private void swapNotesOrPinnedOfUser() {
        if(Boolean.parseBoolean(userModel.getIsCheck())){
            imvPinDetail.setImageResource(R.drawable.ic_pin_detail);
            userModel.setIsCheck("false");
        }else if (Boolean.parseBoolean(userModel.getIsCheck())==false){
            imvPinDetail.setImageResource(R.drawable.ic_unpin_detail);
            userModel.setIsCheck("true");
        }
    }

    private void showDialogCheckboxTextDetail() {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_custom_dialog_checkbox_detail);
        dialog.setCanceledOnTouchOutside(false);
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView tvSendCheckboxDetail = dialog.findViewById(R.id.tv_send_checkbox_detail);
        TextView tvNoCheckBoxDetail = dialog.findViewById(R.id.tv_no_checkbox_detail);
        EditText edtTextCheckboxDetail = dialog.findViewById(R.id.edt_text_checkbox_detail);

        tvNoCheckBoxDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tvSendCheckboxDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = edtTextCheckboxDetail.getText().toString().trim();
                if(text.equals("")){
                    edtTextCheckboxDetail.setText("");
                    edtTextCheckboxDetail.setHint("Please enter a name");
                }else{
                    int id = userModel.getId();
                    mModel.insertCheckboxModel(new CheckboxModel(text,"false",id));

                    listCheckboxModel  = mModel.getListCheckbox(id);
                    checkBoxAdapter.setNewData(listCheckboxModel);

                    dialog.dismiss();
                }
            }
        });
        dialog.show();

    }

    // Trở về màn hình HomeFragment
    private void backFragment() {
        // Đối với những thằng pinned , notes đã có dữ liệu rồi sẽ chỉ cần back lại
        if(MainAct.KEY_BUNDLE.equals(MainAct.KEYBUNDLE_PINNEDADAPTER_DETAIL)||MainAct.KEY_BUNDLE.equals(MainAct.KEYBUNDLE_NOTESADAPTER_DETAIL)){
            getFragmentManager().popBackStack();;
        }else{
            // Đối với thằng create sẽ phải xóa hết checkbox , picture , paint và cả userModel đi xong rồi mới đc back
            mModel.deleteUser(userModel);
            mModel.deleteListPaintModel(listPaintModel);
            mModel.deleteListCheckBoxModel(listCheckboxModel);
            mModel.deleteListPictureModel(listPictureModel);
            getFragmentManager().popBackStack();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void createNotes() {
        String title = edtTitle.getText().toString().trim();
        String content = edtContent.getText().toString().trim();
        // Nếu 1 trong 2 cái = "" thì thông báo là cần nhập đủ
        if(title.equals("")||content.equals("")){
            showDialogError();
        }else{
            // trường hợp đúng
            successNotes();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void successNotes() {
        String title = edtTitle.getText().toString();
        String content = edtContent.getText().toString();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDateTime dateTime = LocalDateTime.now();
        // set lại các giá trị cho userModel
        userModel.setTitle(title);
        userModel.setContent(content);
        userModel.setDate(dtf.format(dateTime));
        // update cho userModel và back lại homeFragment
        mModel.updateUser(userModel);
        getFragmentManager().popBackStack();
    }

    private void showDialogError() {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_custom_dialog_detail);
        dialog.setCancelable(false);
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView tvOK = dialog.findViewById(R.id.tv_ok_detail);
        tvOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    @Override
    public void onCallBackCheckbox(String key, CheckboxModel checkboxModel) {
        switch (key){
            // Chạm 1 lần nhẹ sẽ update checkbox
            case CheckBoxAdapter.KEY_CHECKBOXADAPTER_TO_DETAIL_ONECLICK:
                updateCheckboxModelOneClick(checkboxModel);
                break;
            // Khi chạm lâu sẽ hiện ra menu , delete , repair để lựa chọn vì lúc đầu mặc định gone (Adapter)
            case CheckBoxAdapter.KEY_DELETE_CHECKBOXADAPTER:
                deleteCheckboxModel(checkboxModel);
                break;
            case CheckBoxAdapter.KEY_REPAIR_CHECKBOXADAPTER:
                repairCheckboxModel(checkboxModel);
                break;

        }
    }

    private void repairCheckboxModel(CheckboxModel checkboxModel) {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_custom_repair_checkbox_detail);
        dialog.setCancelable(false);
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        EditText edtTitleCheckboxDetail = dialog.findViewById(R.id.edt_title_checkbox_detail);
        edtTitleCheckboxDetail.setText(checkboxModel.getTextCheck());

        dialog.findViewById(R.id.tv_cancel_checkbox_detail).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.tv_ok_checkbox_detail).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str =  edtTitleCheckboxDetail.getText().toString();
                checkboxModel.setTextCheck(str);
                mModel.updateCheckboxModel(checkboxModel);
                List<CheckboxModel> listCheckboxModel = mModel.getListCheckbox(userModel.getId());
                checkBoxAdapter.setNewData(listCheckboxModel);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void deleteCheckboxModel(CheckboxModel checkboxModel) {
        mModel.deleteCheckboxModel(checkboxModel);
        List<CheckboxModel> listCheckbox = mModel.getListCheckbox(userModel.getId());
        checkBoxAdapter.setNewData(listCheckbox);
    }

    private void updateCheckboxModelOneClick(CheckboxModel checkboxModel) {
        // Nếu ấn one click sẽ thay đổi isCheck của userModel
        if(checkboxModel.getIsCheck().equals("true")){
            checkboxModel.setIsCheck("false");
        }else{
            checkboxModel.setIsCheck("true");
        }
        // update trong database
        mModel.updateCheckboxModel(checkboxModel);
        // set lại list cho userModel và hiển thị lên
        listCheckboxModel  = mModel.getListCheckbox(userModel.getId());
        checkBoxAdapter.setNewData(listCheckboxModel);
    }

    @Override
    public void onCallBackPicture(String key, PictureModel pictureModel) {
        switch (key){
            case PictureAdapter.KEY_DELETE_PICTURE_DETAIL:
                deletePictureModel(pictureModel);
                break;
            case PictureAdapter.KEY_PICTUREADAPTER_TO_DETAIL_ONECLICK:
                showPictureFullScreen(pictureModel);
                break;
        }
    }


    private void showPictureFullScreen(PictureModel pictureModel) {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        dialog.setCanceledOnTouchOutside(false);
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.layout_show_fullscreen_picture_detail);
        ImageView imvPictureDialogDetail = dialog.findViewById(R.id.imv_picture_dialog_detail);
        imvPictureDialogDetail.setImageURI(Uri.parse(pictureModel.getUri()));

        imvPictureDialogDetail.setOnClickListener(new View.OnClickListener() {
            int count = 0 ;
            @Override
            public void onClick(View v) {
                count++;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        count = 0;
                    }
                },500);
                if(count==2){
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }

    private void deletePictureModel(PictureModel pictureModel) {
        mModel.deletePictureModel(pictureModel);
        listPictureModel = mModel.getListPicture(userModel.getId());
        pictureAdapter.setNewData(listPictureModel);
    }

    @Override
    public void onCallBackPaint(String key, PaintModel paintModel) {
        switch (key){
            case PaintAdapter.KEY_DELETE_PAINT_DETAIL:
                deletePaintModel(paintModel);
                break;
            case PaintAdapter.KEY_SHOW_PAINT_DETAIL:
                showPaintFullScreen(paintModel);
        }
    }

    private void showPaintFullScreen(PaintModel paintModel) {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.layout_show_fullscreen_paint_detail);
        ImageView imvPaintDialogDetail = dialog.findViewById(R.id.imv_paint_dialog_detail);
        imvPaintDialogDetail.setImageURI(Uri.parse(paintModel.getPath()));

        imvPaintDialogDetail.setOnClickListener(new View.OnClickListener() {
            int count = 0;
            @Override
            public void onClick(View v) {
                count++;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        count = 0;
                    }
                },500);
                if(count==2){
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }

    private void deletePaintModel(PaintModel paintModel) {
        mModel.deletePaintModel(paintModel);
        List<PaintModel> listPaintModel=  mModel.getListPaint(userModel.getId());
        paintAdapter.setNewData(listPaintModel);
    }


}
