package com.example.inote.ui.fragment.detail;

import android.content.Context;

import com.example.inote.base.BaseViewModel;
import com.example.inote.datasource.database.UserDao;
import com.example.inote.datasource.database.UserDatabase;
import com.example.inote.datasource.model.CheckboxModel;
import com.example.inote.datasource.model.PaintModel;
import com.example.inote.datasource.model.PictureModel;
import com.example.inote.datasource.model.UserModel;

import java.util.List;

public class DetailViewModel extends BaseViewModel {
    private UserDao userDao;
    private Context context;

    public void setContext(Context context) {
        this.context = context;
        userDao = UserDatabase.getInstance(context).userDao();
    }

    public void insertUser(UserModel userModel){
        userDao.insertUser(userModel);
    }

    public void deleteUser(UserModel userModel){
        userDao.deleteUser(userModel);
    }

    public List<UserModel> getListUser(){
        return userDao.listUser();
    }

    public void updateUser(UserModel userModel){
        userDao.updateUser(userModel);
    }

    public List<CheckboxModel> getListCheckbox(int id){
        return userDao.listCheckBoxModel(id);
    }

    public void insertCheckboxModel(CheckboxModel checkboxModel){
        userDao.insertCheckboxModel(checkboxModel);
    }

    public void deleteListCheckBoxModel(List<CheckboxModel> list){
        userDao.deleteListCheckBoxModel(list);
    }

    public void updateCheckboxModel(CheckboxModel checkboxModel){
        userDao.updateCheckboxModel(checkboxModel);
    }

    public void deleteCheckboxModel(CheckboxModel checkboxModel){
        userDao.deleteCheckboxModel(checkboxModel);
    }

    public List<PictureModel> getListPicture(int id){
        return userDao.listPictureModel(id);
    }

    public void deleteListPictureModel(List<PictureModel> listPictureModel) {
        userDao.deleteListPictureModel(listPictureModel);
    }

    public void insertPictureModel(PictureModel pictureModel){
        userDao.insertPictureModel(pictureModel);
    }

    public void deletePictureModel(PictureModel pictureModel){
        userDao.deletePictureModel(pictureModel);
    }

    public List<PaintModel> getListPaint(int id){
        return userDao.mListPaintModel(id);
    }

    public void  deleteListPaintModel(List<PaintModel> listPaintModel){
        userDao.deleteListPaintModel(listPaintModel);
    }

    public void deletePaintModel(PaintModel paintModel){
        userDao.deletePaintModel(paintModel);
    }
}
