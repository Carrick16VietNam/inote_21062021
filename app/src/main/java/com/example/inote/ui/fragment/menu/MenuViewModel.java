package com.example.inote.ui.fragment.menu;

import android.content.Context;

import com.example.inote.base.BaseViewModel;
import com.example.inote.datasource.database.UserDao;
import com.example.inote.datasource.database.UserDatabase;
import com.example.inote.datasource.model.UserModel;

import java.util.List;

public class MenuViewModel extends BaseViewModel {
    private UserDao userDao;
    private Context context;

    public void setContext(Context context) {
        this.context = context;
        userDao = UserDatabase.getInstance(context).userDao();
    }

    public List<UserModel> getListUserModel(){
        return userDao.listUser();
    }

    public List<UserModel> getListUserModelPinned(String str){
        return userDao.listPinnedOrNotes(str);
    }
}
