package com.example.inote.ui.fragment.home;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.inote.R;
import com.example.inote.datasource.model.UserModel;
import com.example.inote.ui.callback.OnActionCallbackFragment;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.List;

public class NotesHomeAdapter extends RecyclerView.Adapter<NotesHomeAdapter.ViewHolder> {
    public static final String KEY_NOTESADAPTER_TO_HOMEFRAGMENT = "KEY_NOTESADAPTER_TO_HOMEFRAGMENT" ;
    public static final String KEY_NOTESADAPTER_TO_HOMEFRAGMENT_LONGCLICK = "KEY_NOTESADAPTER_TO_HOMEFRAGMENT_LONGCLICK";
    private List<UserModel> mList;
    private Context mContext;
    private OnActionCallbackFragment callback;
    private FirebaseAnalytics mFirebaseAnalytics;

    public void setNewData(List<UserModel> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }

    //    public int clickPosition;

    public NotesHomeAdapter(List<UserModel> mList, Context mContext) {
        this.mList = mList;
        this.mContext = mContext;
    }

    public void setCallback(OnActionCallbackFragment callback) {
        this.callback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_item_note_home,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        UserModel userModel = mList.get(position);
        holder.tvTitle.setText(userModel.getTitle());
        holder.tvContent.setText(userModel.getContent());
        holder.tvDate.setText(userModel.getDate());
        Log.i("abc",userModel.getId()+"");

        if(mList.size()==1){
            holder.constraintLayoutItemHome.setBackgroundResource(R.drawable.custom_item_notes_full);
        }else{
            if(position==0){
                holder.constraintLayoutItemHome.setBackgroundResource(R.drawable.custom_item_notes_top);
            }

            if(position==mList.size()-1){
                holder.constraintLayoutItemHome.setBackgroundResource(R.drawable.custom_item_notes_bottom);
            }

            if(position!=0&&position!=mList.size()-1){
                holder.constraintLayoutItemHome.setBackgroundResource(R.drawable.custom_item_notes_center);
            }
        }


        holder.viewBorderItemHome.setBackgroundResource(R.color.gray_black_nhat);
        if(position==mList.size()-1){
            holder.viewBorderItemHome.setBackgroundResource(R.color.white);
        }


        
        holder.constraintLayoutItemHome.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View view) {
                mFirebaseAnalytics = FirebaseAnalytics.getInstance(mContext);
                Bundle params = new Bundle();
                //Check event
                params.putString("event_type","click_Iteam_note");
                mFirebaseAnalytics.logEvent("All_Note_Layout",params);
                callback.onCallback(KEY_NOTESADAPTER_TO_HOMEFRAGMENT,userModel);
            }
        });

        holder.constraintLayoutItemHome.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                callback.onCallback(KEY_NOTESADAPTER_TO_HOMEFRAGMENT_LONGCLICK,userModel);
                return true;
            }
        });




    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        TextView tvContent;
        TextView tvDate;
        ConstraintLayout constraintLayoutItemHome;
        View  viewBorderItemHome;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title_item_home);
            tvContent = itemView.findViewById(R.id.tv_content_item_home);
            tvDate = itemView.findViewById(R.id.tv_date_item_home);
            constraintLayoutItemHome = itemView.findViewById(R.id.constrain_layout_item_home);
            viewBorderItemHome = itemView.findViewById(R.id.view_border_item_home);
        }
    }
}
