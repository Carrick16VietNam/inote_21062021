package com.example.inote.ui.callback;

public interface OnActionCallbackFragment {
    void onCallback(String key , Object object);
}
