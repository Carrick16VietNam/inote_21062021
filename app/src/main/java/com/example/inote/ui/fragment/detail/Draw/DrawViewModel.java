package com.example.inote.ui.fragment.detail.Draw;

import android.content.Context;

import com.example.inote.base.BaseViewModel;
import com.example.inote.datasource.database.UserDao;
import com.example.inote.datasource.database.UserDatabase;
import com.example.inote.datasource.model.PaintModel;

public class DrawViewModel extends BaseViewModel {
    private UserDao userDao;
    private Context context;

    public void setContext(Context context) {
        this.context = context;
        userDao = UserDatabase.getInstance(context).userDao();
    }

    public void insertPaintModel(PaintModel paintModel){
        userDao.insertPaintModel(paintModel);
    }
}
