package com.example.inote.ui.fragment.home;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.inote.R;
import com.example.inote.base.BaseFragment;
import com.example.inote.datasource.model.UserModel;
import com.example.inote.ui.callback.OnActionCallbackFragment;
import com.example.inote.utils.admanager.AdManager;
import com.example.inote.utils.app.App;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator;

  public class  HomeFragment extends BaseFragment<HomeViewModel> implements OnActionCallbackFragment {
      public static final String KEY_MENU_FRAGMENT = "KEY_MENU_FRAGMENT";
      public static final String KEY_DETAIL_FRAGMENT = "KEY_DETAIL_FRAGMENT";
      private static final String KEY_SHAREDPRE = "KEY_SHAREDPRE";
      private static final String KEY_SORT = "KEY_SORT";
      public static final String KEY_HOMEFRAGMENT_TO_MAIN_TO_DETAILFRAGMENT_PIN = "KEY_HOMEFRAGMENT_TO_MAIN_TO_DETAILFRAGMENT_PIN";
      public static final String KEY_HOMEFRAGMENT_TO_MAIN_TO_DETAILFRAGMENT_NOTES = "KEY_HOMEFRAGMENT_TO_MAIN_TO_DETAILFRAGMENT_NOTES";
      private NestedScrollView nestedScrollView;
      private NotesHomeAdapter notesHomeAdapter;
      private PinnedHomeAdapter pinnedHomeAdapter;
      private RecyclerView recyclerViewNotesHome , recyclerViewPinnedHome;
      private TextView tvSumNotesHome , tvNotesHomeTop , tvNoNotesHome , tvPinnedHome;
      private List<UserModel> mListNotes , mListPinned;
      private EditText edtSearchHome;
      private OnActionCallbackFragment callbackFragment;
      private SharedPreferences sharedPreferences;
      private TextView tvDefault2Home;

      private String strSearch = "";
      private RecyclerView recyclerViewNotesHomeSearch , recyclerViewPinnedHomeSearch ;

      private FirebaseAnalytics mFirebaseAnalytics;



      @Override
    protected Class getClassModel() {
        return HomeViewModel.class;
    }

      @Override
      protected int getLayoutId() {
          return R.layout.frag_home;
      }



      public void setCallback(OnActionCallbackFragment callbackFragment) {
        this.callbackFragment = callbackFragment;
    }

    @SuppressLint("MissingPermission")
    @Override
    protected void initViews() {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());

          // Mark each screen
          App.getInstance().setMarkEachScreen(2);
          // Create object userDao using query database
          mModel.setContext(getContext());

          // NestedScrollView using drag layout include adapter
          nestedScrollView = findViewById(R.id.nested_scrollview_home);

          // Show sumNote + sumPinned
          tvSumNotesHome = findViewById(R.id.tv_sum_notes_home);

          // Drag NestedScrollView : case1 scrollY < 30 => tvSumNotesHome INVISIBLE
          tvNotesHomeTop = findViewById(R.id.tv_notes_home);

          // When listNotes.size() = 0 => tvNoNotesHome INVISIBLE , listNotes.size() > 0 => tvNoNotesHome VISIBLE
          tvNoNotesHome = findViewById(R.id.tv_no_notes_home);

          // When listPinned.size() = 0 => tvPinnedHome INVISIBLE , listPinned.size() > 0 => tvPinnedHome VISIBLE
          tvPinnedHome = findViewById(R.id.tv_pinned_home);

          // mListNotes display list NotesHome , mListPinned display list PinnedHome
          mListNotes = new ArrayList<>();
          mListPinned = new ArrayList<>();

          // thằng này ánh xạ chỉ để ẩn di khi ấn vào pinned_menu
          tvDefault2Home = findViewById(R.id.tv_default_2_home);

          //  recyclerViewNotesHome , recyclerViewPinnedHome show list VERTICAL
          recyclerViewNotesHome = findViewById(R.id.recycler_view_notes_home);
          recyclerViewPinnedHome = findViewById(R.id.recycler_view_pinned_home);
          recyclerViewPinnedHome.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false));
          recyclerViewNotesHome.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false));

        recyclerViewNotesHomeSearch = findViewById(R.id.recycler_view_notes_home_search);
        recyclerViewPinnedHomeSearch = findViewById(R.id.recycler_view_pinned_home_search);
        recyclerViewPinnedHomeSearch.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false));
        recyclerViewNotesHomeSearch.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false));

          // Handling event buttons
          findViewById(R.id.constrain_layout_create_home,this);
          findViewById(R.id.constrain_layout_sort_home,this);
          findViewById(R.id.imv_back_home,this);
          findViewById(R.id.constrain_layout_back_home,this);
          edtSearchHome = findViewById(R.id.edt_search_home);

          // Using SharedPreferences save cách sort
          sharedPreferences = getContext().getSharedPreferences(KEY_SHAREDPRE, Context.MODE_PRIVATE);

          // When inside DetailFragment create User new but not done and click back or finish app
          // => Remove User new because when create User new inside HomeFragment changed save database
          // => click back or finish app then User new error
          closeAppSuddenlyOrBackButtonBelow();
          // Xem click vào pinned_menu hay all_notes_menu
          checkClickPinnedMenuOrAllNotesMenu();
          // showListNote and showListPinned
          showListNotesHomeAndPinnedHome();
          clickNestedScrollView();
          setSumNotesBottom();
          visibilityNoNotesHome();
          eventHandlingSearch();

        // Claw Right or Left of NotesHome
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallbackNotesHome);
        itemTouchHelper.attachToRecyclerView(recyclerViewNotesHome);
        // Claw Right or Left of Pinned
        ItemTouchHelper itemTouchHelper1 = new ItemTouchHelper(simpleCallbackPinnedHome);
        itemTouchHelper1.attachToRecyclerView(recyclerViewPinnedHome);
    }

      private void checkClickPinnedMenuOrAllNotesMenu() {
            boolean result = App.getInstance().isCheckPinnedorAllNotesMenu();
            if(result==false){
                tvNoNotesHome.setVisibility(View.GONE);
                tvDefault2Home.setVisibility(View.GONE);
                recyclerViewNotesHome.setVisibility(View.GONE);
                tvSumNotesHome.setVisibility(View.GONE);
                tvPinnedHome.setVisibility(View.VISIBLE);
            }
      }



      private void closeAppSuddenlyOrBackButtonBelow() {
        // Khi nhấn nút create thì đã tạo mới thằng User với title , content = "" và đã lưu vào database
        // Khi mà thoát app đột ngột hoặc back lại thì tức là chưa lưu và để nó k tồn tại trong list user
                    // thì phải xóa nó .Tìm title = "" , content = ""  để xóa trong list user
          List<UserModel> list = mModel.getListUserModel();
          for(UserModel userModel : list){
                if(userModel.getTitle().equals("")||userModel.getContent().equals("")){
                    mModel.deleteUser(userModel);
                    break;
                }
          }
      }

      // Xử lý sự kiện của edittext để tìm kiếm các Notes , Pinned
    private void eventHandlingSearch() {
          Bundle params = new Bundle();
         edtSearchHome.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 //Check event
                 params.putString("event_type","click_Search");
                 mFirebaseAnalytics.logEvent("All_Note_Layout",params);
             }
         });


        edtSearchHome.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable) {
                recyclerViewNotesHomeSearch.setVisibility(View.VISIBLE);
                recyclerViewPinnedHomeSearch.setVisibility(View.VISIBLE);
                recyclerViewNotesHome.setVisibility(View.GONE);
                recyclerViewPinnedHome.setVisibility(View.GONE);
                findViewById(R.id.include_bottom_home).setVisibility(View.GONE);
                strSearch = editable+"";

                // Tạo listNotes và listPinned để hứng các giá trị
                List<UserModel> listNotesSearch = new ArrayList<>();
                List<UserModel> listPinnedSearch = new ArrayList<>();


                // Hứng các giá trị đúng  khi tìm kiếm qua editable
                listNotesSearch = mModel.searchUserModel(""+editable,"true");
                listPinnedSearch = mModel.searchUserModel(""+editable,"false");

                // Đổ dữ liệu vào recyclerView
                pinnedHomeAdapter.setNewData(listPinnedSearch);
                recyclerViewPinnedHomeSearch.setAdapter(pinnedHomeAdapter);
                notesHomeAdapter.setNewData(listNotesSearch);
                recyclerViewNotesHomeSearch.setAdapter(notesHomeAdapter);

                boolean result = App.getInstance().isCheckPinnedorAllNotesMenu();
                if(result==false){
                    recyclerViewNotesHomeSearch.setVisibility(View.GONE);
                }
                

                // Trường hợp k còn tìm gì nữa sẽ trả về danh sách
                if((editable+"").equals("")){
                    recyclerViewNotesHome.setVisibility(View.VISIBLE);
                    recyclerViewNotesHomeSearch.setVisibility(View.GONE);
                    recyclerViewPinnedHomeSearch.setVisibility(View.GONE);
                    findViewById(R.id.include_bottom_home).setVisibility(View.VISIBLE);

                    int index = sharedPreferences.getInt(KEY_SORT,3);
                    // Khi mà ấn sort rôi sẽ lưu giá trị index để khi vào app nó đọ giá trị và sẽ lưu tương ứng với index
                    if(index ==3 ) {
                        // Sort original
                        // get listPinned from database when the isCheck = false
                        mListPinned = mModel.getListPinnedOrNotes("false");
                        // get listNotes from database when the isCheck = true
                        mListNotes = mModel.getListPinnedOrNotes("true");
                    } else if(index == 2){
                        // Sort ZA
                        mListPinned = mModel.sortZA("false");
                        mListNotes = mModel.sortZA("true");
                    } else if(index == 1 ){
                        // Sort AZ
                        mListPinned = mModel.sortAZ("false");
                        mListNotes = mModel.sortAZ("true");
                    }


                    // Khi vào lại app nếu mListPinned.size > 0 thì sẽ hiển thị lên còn nếu mListPinned.size = 0 thì mặc định
                    // trong xml là GONE
                    if(mListPinned.size()>0){
                        recyclerViewPinnedHome.setVisibility(View.VISIBLE);
                        tvPinnedHome.setVisibility(View.VISIBLE);
                    }


                    notesHomeAdapter.setNewData(mListNotes);
                    recyclerViewNotesHome.setAdapter(notesHomeAdapter);
                    pinnedHomeAdapter.setNewData(mListPinned);
                    recyclerViewPinnedHome.setAdapter(pinnedHomeAdapter);

                    boolean result1 = App.getInstance().isCheckPinnedorAllNotesMenu();
                    if(result1==false){
                        recyclerViewNotesHome.setVisibility(View.GONE);
                    }

                }
            }
        });
    }

    // Display No Note Home in mlistNotes
    private void visibilityNoNotesHome() {

        if(mModel.getListPinnedOrNotes("true").size()==0){
            tvNoNotesHome.setVisibility(View.VISIBLE);
        }else{
            tvNoNotesHome.setVisibility(View.GONE);
        }

        boolean result = App.getInstance().isCheckPinnedorAllNotesMenu();
        if(result==false){
            tvNoNotesHome.setVisibility(View.GONE);
        }
    }

    // Hiển thị tổng số nút của cả Notes và Pinned
    private void setSumNotesBottom() {
        tvSumNotesHome.setText(mModel.getListUserModel().size()+" notes");
    }

    private void showListNotesHomeAndPinnedHome() {
        // Nếu như lúc đầu down app về thì  sort sẽ là sort original (vì defValue = 3)
        int index = sharedPreferences.getInt(KEY_SORT,3);

        // Khi mà ấn sort rôi sẽ lưu giá trị index để khi vào app nó đọ giá trị và sẽ lưu tương ứng với index
        if(index ==3 ) {
            // Sort original
            // get listPinned from database when the isCheck = false
            mListPinned = mModel.getListPinnedOrNotes("false");
            // get listNotes from database when the isCheck = true
            mListNotes = mModel.getListPinnedOrNotes("true");
        } else if(index == 2){
            // Sort ZA
            mListPinned = mModel.sortZA("false");
            mListNotes = mModel.sortZA("true");
        } else if(index == 1 ){
            // Sort AZ
            mListPinned = mModel.sortAZ("false");
            mListNotes = mModel.sortAZ("true");
        }


        // Khi vào lại app nếu mListPinned.size > 0 thì sẽ hiển thị lên còn nếu mListPinned.size = 0 thì mặc định
               // trong xml là GONE
        if(mListPinned.size()>0){
            recyclerViewPinnedHome.setVisibility(View.VISIBLE);
            tvPinnedHome.setVisibility(View.VISIBLE);
        }

          // Hiển thị list Pinned , list Notes lên RecyclerView và hứng giá trị callback để lấy User từ
                    // NotesAdapter hoặc PinnedAdapter sang bên HomeFragment
          pinnedHomeAdapter = new PinnedHomeAdapter(getContext(),mListPinned);
          pinnedHomeAdapter.setCallback(this);
          recyclerViewPinnedHome.setAdapter(pinnedHomeAdapter);
          notesHomeAdapter = new NotesHomeAdapter(mListNotes,getContext());
          notesHomeAdapter.setCallback(this);
          recyclerViewNotesHome.setAdapter(notesHomeAdapter);
    }

        // Xử lý sự kiện khi kéo NestedScrollView để hiển thị NotesHomeTop
    private void clickNestedScrollView() {
        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY < 30) {
                    tvNotesHomeTop.setVisibility(View.INVISIBLE);
                } else {
                    tvNotesHomeTop.setVisibility(View.VISIBLE);
                }
            }
        });
    }


        // ItemTouchHelper use claw Right or Left of NotesHome
        ItemTouchHelper.SimpleCallback simpleCallbackNotesHome = new ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.LEFT|ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                // Lấy vị trí đang Claw
                int position = viewHolder.getAdapterPosition();
                switch (direction){
                    case ItemTouchHelper.LEFT:
                            clawRightToLeftOfNotesHome(position);
                        break;
                    case ItemTouchHelper.RIGHT:
                            clawLeftToRightOfNotesHome(position);
                        break;
                }
            }

            // setColor vs setIcon of SwipeLeft and SwipeRight in NotesHome
            @Override
            public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                new RecyclerViewSwipeDecorator.Builder(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                        .addSwipeLeftBackgroundColor(ContextCompat.getColor(getContext(), R.color.red))
                        .addSwipeLeftActionIcon(R.drawable.ic_delete)
                        .addSwipeRightBackgroundColor(ContextCompat.getColor(getContext(), R.color.background_icon))
                        .addSwipeRightActionIcon(R.drawable.ic_pin_bottom)
                        .create()
                        .decorate();
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };

        // ItemTouchHelper use claw Right or Left of PinnedHome
        ItemTouchHelper.SimpleCallback simpleCallbackPinnedHome = new ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.LEFT|ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                switch (direction){
                    case ItemTouchHelper.LEFT:
                        clawRightToLeftOfPinnedHome(position);
                        break;
                    case ItemTouchHelper.RIGHT:
                        clawLeftToRightOfPinnedHome(position);
                        break;
                }
            }

            @Override
            public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                new RecyclerViewSwipeDecorator.Builder(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                        .addSwipeLeftBackgroundColor(ContextCompat.getColor(getContext(), R.color.red))
                        .addSwipeLeftActionIcon(R.drawable.ic_delete)
                        .addSwipeRightBackgroundColor(ContextCompat.getColor(getContext(), R.color.background_icon))
                        .addSwipeRightActionIcon(R.drawable.ic_pin_top)
                        .create()
                        .decorate();
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };

    private void clawLeftToRightOfPinnedHome(int position) {
        int index = sharedPreferences.getInt(KEY_SORT,3);
        if(index==3){
            UserModel userModel = mModel.getListPinnedOrNotes("false").get(position);
            userModel.setIsCheck("true");
            mModel.updateUser(userModel);

            // get listPinned from database when the isCheck = false
            mListPinned = mModel.getListPinnedOrNotes("false");
            // get listNotes from database when the isCheck = true
            mListNotes = mModel.getListPinnedOrNotes("true");
        } else if(index == 2){
            UserModel userModel = mModel.sortZA("false").get(position);
            userModel.setIsCheck("true");
            mModel.updateUser(userModel);

            mListNotes = mModel.sortZA("true");
            mListPinned = mModel.sortZA("false");
        } else if(index == 1){
            UserModel userModel = mModel.sortAZ("false").get(position);
            userModel.setIsCheck("true");
            mModel.updateUser(userModel);

            mListNotes = mModel.sortAZ("true");
            mListPinned = mModel.sortAZ("false");
        }

        if(mListPinned.size()>0){
            recyclerViewPinnedHome.setVisibility(View.VISIBLE);
            tvPinnedHome.setVisibility(View.VISIBLE);
            pinnedHomeAdapter.setNewData(mListPinned);
        }else{
            recyclerViewPinnedHome.setVisibility(View.GONE);
            tvPinnedHome.setVisibility(View.GONE);
        }

        if(mListNotes.size()>0){
            tvNoNotesHome.setVisibility(View.GONE);
            notesHomeAdapter.setNewData(mListNotes);
        }



    }

    private void clawRightToLeftOfPinnedHome(int position) {
        int index = sharedPreferences.getInt(KEY_SORT,3);
        if(index == 3){
            UserModel userModel = mModel.getListPinnedOrNotes("false").get(position);
//        UserModel userModel = mModel.sortAZ("false").get(position);
            mModel.deleteUser(userModel);
            mListPinned = mModel.getListPinnedOrNotes("false");
        }else if(index == 2){
            UserModel userModel = mModel.sortZA("false").get(position);
            mModel.deleteUser(userModel);
            mListPinned = mModel.sortZA("false");
        }else if(index == 1){
            UserModel userModel = mModel.sortAZ("false").get(position);
            mModel.deleteUser(userModel);
            mListPinned = mModel.sortAZ("false");
        }

        if(mListPinned.size()>0){
            recyclerViewPinnedHome.setVisibility(View.VISIBLE);
            tvPinnedHome.setVisibility(View.VISIBLE);
            pinnedHomeAdapter.setNewData(mListPinned);
        }else{
            recyclerViewPinnedHome.setVisibility(View.GONE);
            tvPinnedHome.setVisibility(View.GONE);
        }

        setSumNotesBottom();
    }

    // Add userModel Pinned , remove userModel Notes
    private void clawLeftToRightOfNotesHome(int position) {
        // Tương tự như remove userModel NotesHome
        int index = sharedPreferences.getInt(KEY_SORT,3);
        if(index == 3){
            // get userModel in listNotes index position
            UserModel userModel = mModel.getListPinnedOrNotes("true").get(position);
            // update userModel => userModel in listPinned
            userModel.setIsCheck("false");
            mModel.updateUser(userModel);

            // get listPinned from database when the isCheck = false
            mListPinned = mModel.getListPinnedOrNotes("false");
            // get listNotes from database when the isCheck = true
            mListNotes = mModel.getListPinnedOrNotes("true");
        }else if(index == 2){
            UserModel userModel = mModel.sortZA("true").get(position);
            userModel.setIsCheck("false");
            mModel.updateUser(userModel);

            mListPinned = mModel.sortZA("false");
            mListNotes = mModel.sortZA("true");
        }else if(index == 1){
            UserModel userModel = mModel.sortAZ("true").get(position);
            userModel.setIsCheck("false");
            mModel.updateUser(userModel);

            mListPinned = mModel.sortAZ("false");
            mListNotes = mModel.sortAZ("true");
        }


        // Cứ mỗi lần claw thì xem mListPinned.size()>0 hay không? nếu >0 thì để VISIBLE
        if(mListPinned.size()>0){
            recyclerViewPinnedHome.setVisibility(View.VISIBLE);
            tvPinnedHome.setVisibility(View.VISIBLE);
            pinnedHomeAdapter.setNewData(mListPinned);
        }

        notesHomeAdapter.setNewData(mListNotes);

        // If mListNotes.size = 0 => Appear No Notes  else mListNotes.size > 0 => Show list
        visibilityNoNotesHome();
    }
    // Remove UserModel of the list Notes
    private void clawRightToLeftOfNotesHome(int position) {
        // Để xóa phần tử tại vị trí position thì phải biết nó đang sort theo kiểu nào?
        // Để nó biết sort theo kiểu nào thì lại xử dụng biến index để so sánh xem nó vào trường hợp nào
        // xóa và update lại list
        int index = sharedPreferences.getInt(KEY_SORT,3);
        if(index == 3){

            // get userModel remove
            UserModel userModel = mModel.getListPinnedOrNotes("true").get(position);
            mModel.deleteUser(userModel);
            mListNotes = mModel.getListPinnedOrNotes("true");
        }else if(index == 2){

            UserModel userModel = mModel.sortZA("true").get(position);
            mModel.deleteUser(userModel);
            mListNotes = mModel.sortZA("true");
        }else if(index == 1){

            UserModel userModel = mModel.sortAZ("true").get(position);
            mModel.deleteUser(userModel);
            mListNotes = mModel.sortAZ("true");
        }

        // update lại list và dổ lên RecyclerView
        notesHomeAdapter.setNewData(mListNotes);

        // Update sum Notes Bottom
        setSumNotesBottom();

        // If mListNotes.size = 0 => Appear No Notes  else mListNotes.size > 0 => Show list
        visibilityNoNotesHome();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onClick(View v) {
        Bundle params = new Bundle();
        if(v.getId()==R.id.constrain_layout_create_home){
            //Check event
            params.putString("event_type","click_New_note");
            mFirebaseAnalytics.logEvent("All_Note_Layout",params);

            createNotesHome();
        } else if(v.getId()==R.id.constrain_layout_sort_home){
            //Check event
            params.putString("event_type","click_IconMore");
            mFirebaseAnalytics.logEvent("All_Note_Layout",params);

            sortNotesHome();
        } else if(v.getId()==R.id.imv_back_home||v.getId()==R.id.constrain_layout_back_home){
            goToMenuFragment();
        }
     }

     // Chuyển sang MenuFragment bằng nút back
    private void goToMenuFragment() {
        callbackFragment.onCallback(KEY_MENU_FRAGMENT,null);
    }

    private void sortNotesHome() {
        View viewDialog = LayoutInflater.from(getContext()).inflate(R.layout.layout_bottom_sheet_home,null);
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getContext(),R.style.SheetDialog);
        bottomSheetDialog.setContentView(viewDialog);
        bottomSheetDialog.show();
        viewDialog.findViewById(R.id.tv_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });
        viewDialog.findViewById(R.id.tv_sort_a_z).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Tạo ra 2  list để hứng các giá trị tương ứng khi sort trong database
                List<UserModel> listNotes = new ArrayList<>();
                List<UserModel> listPinned = new ArrayList<>();
                listNotes = mModel.sortAZ("true");
                listPinned = mModel.sortAZ("false");

                // set lại data lên RecyclerView
                pinnedHomeAdapter.setNewData(listPinned);
                notesHomeAdapter.setNewData(listNotes);

                // Gán kiểu sort a->z bằng 1 trong SharedPreference
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt(KEY_SORT,1);
                editor.commit();
                bottomSheetDialog.dismiss();


            }
        });

        viewDialog.findViewById(R.id.tv_sort_z_a).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Tương tự như sort a->z
                List<UserModel> listNotes = new ArrayList<>();
                List<UserModel> listPinned = new ArrayList<>();
                listNotes = mModel.sortZA("true");
                listPinned = mModel.sortZA("false");

                // Đổ dữ liệu vào recyclerView
                pinnedHomeAdapter.setNewData(listPinned);
                notesHomeAdapter.setNewData(listNotes);

                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt(KEY_SORT,2);
                editor.commit();
                bottomSheetDialog.dismiss();
            }
        });

        viewDialog.findViewById(R.id.tv_sort_time).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Tương tự như sort a->z
                List<UserModel> listNotes = new ArrayList<>();
                List<UserModel> listPinned = new ArrayList<>();
                listNotes = mModel.getListPinnedOrNotes("true");
                listPinned = mModel.getListPinnedOrNotes("false");

                pinnedHomeAdapter.setNewData(listPinned);
                notesHomeAdapter.setNewData(listNotes);

                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt(KEY_SORT,3);
                editor.commit();
                bottomSheetDialog.dismiss();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void createNotesHome() {
        // Khi ấn vào image createNotes để sang thằng DetailFragment thì sẽ tạo mới 1 thằng user luôn và
                // title = "" , content =  "" , isCheck = true và date = thời gian hiện tại
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.now();
        UserModel userModel = new UserModel("","",dtf.format(dateTime),"true");
        mModel.insertUser(userModel);

        // Chuyển sang DetailFragment bằng image createNotes
        callbackFragment.onCallback(KEY_DETAIL_FRAGMENT,userModel);
    }

      @Override
      public void onCallback(String key, Object object) {
          switch (key){
              // Trường hợp khi ấn vào item PinnedHomeAdapter
              case PinnedHomeAdapter.KEY_PINNEDADAPTER_TO_HOMEFRAGMENT:
                  // Lấy đc giá trị user vừa ấn
                  UserModel userModel = (UserModel) object;
                  // chuyển user vừa ấn từ homeFragment sang bên DetailFragment thông qua Activity
                  callbackFragment.onCallback(KEY_HOMEFRAGMENT_TO_MAIN_TO_DETAILFRAGMENT_PIN,userModel);
                  break;
              // Trường hợp khi ấn vào item NotesHomeAdapter
              case NotesHomeAdapter.KEY_NOTESADAPTER_TO_HOMEFRAGMENT:
                  // Lấy đc giá trị user vừa ấn
                  UserModel userModel1 = (UserModel) object;
                  // chuyển user vừa ấn từ homeFragment sang bên DetailFragment thông qua Activity
                  callbackFragment.onCallback(KEY_HOMEFRAGMENT_TO_MAIN_TO_DETAILFRAGMENT_NOTES,userModel1);
                  break;
                  // xóa , cancel khi tìm kiếm ở notes
              case NotesHomeAdapter.KEY_NOTESADAPTER_TO_HOMEFRAGMENT_LONGCLICK:
                  UserModel userModel2 = (UserModel) object;
                  Dialog dialog = new Dialog(getContext());
                  dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                  dialog.setContentView(R.layout.layout_custom_user_notes_search_home);
                  dialog.setCanceledOnTouchOutside(false);
                  Window window = dialog.getWindow();
                  window.setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.WRAP_CONTENT);
                  window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                  TextView tvCancelSearchNotesHome = dialog.findViewById(R.id.tv_cancel_search_notes_home);
                  tvCancelSearchNotesHome.setOnClickListener(new View.OnClickListener() {
                      @Override
                      public void onClick(View v) {
                          dialog.dismiss();
                      }
                  });

                  TextView tvDeleteSearchNoteHome = dialog.findViewById(R.id.tv_delete_search_notes_home);
                  tvDeleteSearchNoteHome.setOnClickListener(new View.OnClickListener() {
                      @Override
                      public void onClick(View v) {
                          mModel.deleteUser(userModel2);

                          // xóa khi search
                          List<UserModel> listNotesSearch = new ArrayList<>();
                          listNotesSearch = mModel.searchUserModel(""+strSearch,"true");
                          notesHomeAdapter.setNewData(listNotesSearch);
                          recyclerViewNotesHomeSearch.setAdapter(notesHomeAdapter);

                          setSumNotesBottom();
                          dialog.dismiss();

                      }
                  });
                  dialog.show();
                  break;
              case PinnedHomeAdapter.KEY_PINNEDADAPTER_TO_HOMEFRAGMENT_LONGCLICK:
                  UserModel userModel3 = (UserModel) object;
                  Dialog dialog1 = new Dialog(getContext());
                  dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                  dialog1.setContentView(R.layout.layout_custom_user_pinned_search_home);
                  dialog1.setCanceledOnTouchOutside(false);
                  Window window1 = dialog1.getWindow();
                  window1.setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.WRAP_CONTENT);
                  window1.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                  TextView tvCancelSearchPinnedHome = dialog1.findViewById(R.id.tv_cancel_search_pinned_home);
                  tvCancelSearchPinnedHome.setOnClickListener(new View.OnClickListener() {
                      @Override
                      public void onClick(View v) {
                          dialog1.dismiss();
                      }
                  });

                  TextView tvDeleteSearchPinnedHome = dialog1.findViewById(R.id.tv_delete_search_pinned_home);
                  tvDeleteSearchPinnedHome.setOnClickListener(new View.OnClickListener() {
                      @Override
                      public void onClick(View v) {
                          mModel.deleteUser(userModel3);

                          // xóa khi search
                          List<UserModel> listPinnedSearch = new ArrayList<>();
                          listPinnedSearch = mModel.searchUserModel(""+strSearch,"false");
                          pinnedHomeAdapter.setNewData(listPinnedSearch);
                          recyclerViewPinnedHomeSearch.setAdapter(pinnedHomeAdapter);

                          setSumNotesBottom();
                          dialog1.dismiss();
                      }
                  });

                  dialog1.show();
                  break;


          }
      }
  }
