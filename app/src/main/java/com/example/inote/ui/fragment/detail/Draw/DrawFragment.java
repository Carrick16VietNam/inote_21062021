package com.example.inote.ui.fragment.detail.Draw;

import android.Manifest;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.Toast;

import com.example.inote.R;
import com.example.inote.base.BaseFragment;
import com.example.inote.datasource.model.PaintModel;
import com.example.inote.datasource.model.UserModel;
import com.example.inote.ui.act.main.MainAct;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.skydoves.colorpickerview.ColorPickerView;
import com.skydoves.colorpickerview.listeners.ColorListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.UUID;

public class DrawFragment extends BaseFragment<DrawViewModel> {
    private PaintView paintView;
    private UserModel userModel;
    private SeekBar seekBarDrawPaintDetail;
    private SeekBar seekBarEraserPaintDetail;
    private ColorPickerView colorPickerView;
    private boolean isCheckColorEraser;
    private boolean isCheckEventOnTouchListener;
    @Override
    protected Class<DrawViewModel> getClassModel() {
        return DrawViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frag_draw_paint_detail;
    }

    @Override
    protected void initViews() {
        mModel.setContext(getContext());

        checkPermission();

        getDataDetailFragment();

        initDraw();

        initEvents();


    }

    @Override
    public void onResume() {
        super.onResume();
        Window window = getActivity().getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getActivity().getResources().getColor(R.color.white));
    }

    private void initEvents() {
        findViewById(R.id.imv_back_draw_paint_detail,this);
        findViewById(R.id.constraint_layout_back_draw_paint_detail,this);
        findViewById(R.id.constraint_layout_save_draw_paint_detail,this);
        findViewById(R.id.imv_draw_paint_detail,this);
        findViewById(R.id.imv_eraser_paint_detail,this);
        findViewById(R.id.imv_color_paint_detail,this);
        findViewById(R.id.imv_color_paint_detail,this);
        colorPickerView = findViewById(R.id.color_picker_view);
        seekBarDrawPaintDetail = findViewById(R.id.seek_bar_draw_paint_detail);
        seekBarEraserPaintDetail = findViewById(R.id.seek_bar_eraser_paint_detail);

        // Lúc đầu vào mặc định sẽ vào setColorListener luôn
        colorPickerView.setColorListener(new ColorListener() {
            @Override
            public void onColorSelected(int color, boolean fromUser) {
                paintView.brushColor = color;

                // điều kiện để khi ấn xóa nó ra các màu khác
                if(isCheckColorEraser==true){
                    paintView.brushColor = Color.WHITE;
                }

                // Trường hợp khi đầu tiên vào DrawFragment cần gán lại màu nếu không nó sẽ màu trắng
                if(colorPickerView.getVisibility() == View.GONE){
                    paintView.brushColor = Color.BLACK;
                }

            }
        });

    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imv_back_draw_paint_detail:
                getFragmentManager().popBackStack();
                break;
            case R.id.constraint_layout_back_draw_paint_detail:
                getFragmentManager().popBackStack();
                break;
            case R.id.constraint_layout_save_draw_paint_detail:
                savePaintDetail();
                break;
            case R.id.imv_color_paint_detail:
                colorPickerView.setVisibility(View.VISIBLE);
                seekBarDrawPaintDetail.setVisibility(View.GONE);
                seekBarEraserPaintDetail.setVisibility(View.GONE);
                break;
            case R.id.imv_draw_paint_detail:
                drawPaintDetail();
                break;
            case R.id.imv_eraser_paint_detail:
                eraserPaintDetail();
                break;
        }
    }

    private void eraserPaintDetail() {
        seekBarEraserPaintDetail.setVisibility(View.VISIBLE);
        seekBarEraserPaintDetail.setProgress(paintView.brushSize);
        paintView.brushColor = Color.WHITE;
        seekBarDrawPaintDetail.setVisibility(View.GONE);
        colorPickerView.setVisibility(View.GONE);
        isCheckColorEraser = true;
        seekBarEraserPaintDetail.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                paintView.brushSize = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                seekBar.setVisibility(View.GONE);
            }
        });
    }

    private void drawPaintDetail() {
        seekBarDrawPaintDetail.setVisibility(View.VISIBLE);
        seekBarDrawPaintDetail.setProgress(paintView.brushSize);
        seekBarEraserPaintDetail.setVisibility(View.GONE);
        colorPickerView.setVisibility(View.GONE);
        isCheckColorEraser = false;
        if(paintView.brushColor==Color.WHITE){
            paintView.brushColor = Color.BLACK;
        }
        seekBarDrawPaintDetail.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                paintView.brushSize = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                seekBar.setVisibility(View.GONE);
            }
        });
    }

    private void savePaintDetail() {
        if(isCheckEventOnTouchListener==true){
            ByteArrayOutputStream bStream = new ByteArrayOutputStream();
            Bitmap bitmap =paintView.getBitmap();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, (OutputStream)bStream);
            byte[] byteArray = bStream.toByteArray();
            Bitmap bitmap1 = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            String uuid = UUID.randomUUID().toString().replace("-", "");
            String imageDir = Environment.DIRECTORY_PICTURES + "/Android Draw/";
            File path = Environment.getExternalStoragePublicDirectory(imageDir);
            Log.e("path", path.toString());
            Log.e("path",uuid + ".png");
            File file = new File(path, uuid + ".png");
            path.mkdirs();
            try {
                file.createNewFile();
                FileOutputStream outputStream = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, (OutputStream)outputStream);
                outputStream.flush();
                outputStream.close();
                paintView.clear();
            } catch (Exception e) {
                e.printStackTrace();
            }
            String link = path.toString()+"/"+uuid+".png";
            mModel.insertPaintModel(new PaintModel(link,userModel.getId()));
            getFragmentManager().popBackStack();
        }else{
            // Không làm gì
        }

    }

    private void checkPermission() {
        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
            }

            @Override
            public void onPermissionDenied(List<String> deniedPermissions) {
                Toast.makeText(getContext(), "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }
        };

        TedPermission.with(getContext())
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("Check Permission")
                .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA)
                .check();
    }

    private void getDataDetailFragment() {
        Bundle bundle = getArguments();
        userModel = (UserModel) bundle.getSerializable(MainAct.KEY_USER_DIALOG_FRAGMENT);
    }

    private void initDraw() {
        paintView = findViewById(R.id.paintView);
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
        paintView.init(metrics);
        paintView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                isCheckEventOnTouchListener = true;
                return false;
            }
        });
    }


}
