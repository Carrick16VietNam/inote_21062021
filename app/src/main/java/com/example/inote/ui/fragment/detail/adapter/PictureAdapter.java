package com.example.inote.ui.fragment.detail.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.inote.R;
import com.example.inote.datasource.model.PictureModel;
import com.example.inote.ui.callback.OnActionCallBack;

import java.util.List;

public class PictureAdapter extends RecyclerView.Adapter<PictureAdapter.ViewHolder> {
    public static final String KEY_PICTUREADAPTER_TO_DETAIL_ONECLICK = "KEY_PICTUREADAPTER_TO_DETAIL_ONECLICK";
    public static final String KEY_DELETE_PICTURE_DETAIL = "KEY_DELETE_PICTURE_DETAIL";
    private Context mContext;
    private List<PictureModel> mListPicture;
    private OnActionCallBack callBack;

    public PictureAdapter(Context mContext, List<PictureModel> mListPicture) {
        this.mContext = mContext;
        this.mListPicture = mListPicture;
    }

    public void setNewData(List<PictureModel> mList){
        mListPicture = mList;
        notifyDataSetChanged();
    }

    public void setCallBack(OnActionCallBack callBack) {
        this.callBack = callBack;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_item_picture_detail,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PictureModel pictureModel = mListPicture.get(position);
        Uri uri = Uri.parse(pictureModel.getUri());
//        holder.imvPictureDetail.setImageURI(uri);
          Glide.with(mContext).load(uri).into(holder.imvPictureDetail);
        holder.constrainLayoutPictureDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callBack.onCallBackPicture(KEY_PICTUREADAPTER_TO_DETAIL_ONECLICK,pictureModel);
            }
        });

        holder.imvDeletePictureDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.onCallBackPicture(KEY_DELETE_PICTURE_DETAIL,pictureModel);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mListPicture.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imvPictureDetail;
        ConstraintLayout constrainLayoutPictureDetail;
        ImageView imvDeletePictureDetail;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imvPictureDetail = itemView.findViewById(R.id.imv_picture_detail);
            constrainLayoutPictureDetail = itemView.findViewById(R.id.constrain_layout_picture_detail);
            imvDeletePictureDetail = itemView.findViewById(R.id.imv_delete_picture_detail);
        }
    }
}
