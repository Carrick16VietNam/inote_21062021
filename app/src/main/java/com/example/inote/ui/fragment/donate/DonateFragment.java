package com.example.inote.ui.fragment.donate;

import android.view.View;

import com.example.inote.R;
import com.example.inote.base.BaseFragment;

public class DonateFragment extends BaseFragment<DonateViewModel> {
    @Override
    protected Class<DonateViewModel> getClassModel() {
        return DonateViewModel.class ;
    }

    @Override
    protected void initViews() {
        findViewById(R.id.constraint_layout_back_donate,this);
        findViewById(R.id.tv_go_to_menu_donate,this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frag_donate;
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.constraint_layout_back_donate||v.getId()==R.id.tv_go_to_menu_donate){
            getFragmentManager().popBackStack();
        }
    }
}
