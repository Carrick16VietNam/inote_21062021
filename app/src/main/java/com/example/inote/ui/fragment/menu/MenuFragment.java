package com.example.inote.ui.fragment.menu;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


import com.example.inote.R;
import com.example.inote.base.BaseFragment;
import com.example.inote.ui.act.main.MainAct;
import com.example.inote.ui.callback.OnActionCallBackFinishApp;
import com.example.inote.ui.callback.OnActionCallbackFragment;
import com.example.inote.utils.admanager.AdManager;
import com.example.inote.utils.app.App;
import com.google.android.gms.ads.LoadAdError;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.xuandq.rate.ProxRateDialog;
import com.xuandq.rate.RatingDialogListener;

import static android.content.Context.MODE_PRIVATE;

public class MenuFragment extends BaseFragment<MenuViewModel> {
    public static final String KEY_HOME_FRAGMENT1 = "KEY_HOME_FRAGMENT1";
    public static final String KEY_HOME_FRAGMENT_NOT_NOTES = "KEY_HOME_FRAGMENT_NOT_NOTES";
    public static final String KEY_DONATE_FRAGMENT = "KEY_DONATE_FRAGMENT";
    private static final String CHECK_START_APP_2_TURN = "CHECK_START_APP_2_TURN";
    private static final String KEY_BOOLEAN = "KEY_BOOLEAN";
    private TextView tvAllNotes , tvPinned;
    private OnActionCallbackFragment callBack;
    private OnActionCallBackFinishApp callBackFinishApp;
    private FirebaseAnalytics mFirebaseAnalytics;
    private SharedPreferences sharedPreferences;
    private SharedPreferences sharedPreferences1;
    private SharedPreferences.Editor myEdit1;
    @Override
    protected Class<MenuViewModel> getClassModel() {
        return MenuViewModel.class;
    }

    public void setCallBack(OnActionCallbackFragment callBack) {
        this.callBack = callBack;
    }

    public void setCallBackFinishApp(OnActionCallBackFinishApp callBackFinishApp) {
        this.callBackFinishApp = callBackFinishApp;
    }

    @SuppressLint({"MissingPermission", "WrongConstant"})
    @Override
    protected void initViews() {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());

        // Lấy giá trị ở sharedPre để xem khi nào i = 2 sẽ hiện push rate
        sharedPreferences = getContext().getSharedPreferences("MySharedPref",MODE_PRIVATE);
        int i = sharedPreferences.getInt(MainAct.KEY_COUNT_START,0);

        // Check khi rate rồi sẽ k cho rate nữa => dùng sharedPre
        sharedPreferences1 = getContext().getSharedPreferences(CHECK_START_APP_2_TURN,MODE_PRIVATE);
        myEdit1 = sharedPreferences1.edit();
        if(i==2&&sharedPreferences1.getBoolean(KEY_BOOLEAN,false)==false&&MainAct.sharedPreferencesRateOneTurn.getBoolean(MainAct.KEY_RATE,false)==false){
            ProxRateDialog.Config config = new ProxRateDialog.Config();
            config.setListener(new RatingDialogListener() {
                @Override
                public void onSubmitButtonClicked(int rate, String comment) {
                    Bundle params = new Bundle();
                    params.putString("event_type","rated");
                    params.putString("comment",comment);
                    params.putInt("star",rate);
                    mFirebaseAnalytics.logEvent("prox_rating_layout",params);

                    myEdit1.putBoolean(KEY_BOOLEAN,true);
                    myEdit1.apply();

                    MainAct.myEdit2.putBoolean(MainAct.KEY_RATE,true);
                    MainAct.myEdit2.apply();

                }

                @Override
                public void onLaterButtonClicked() {

                }

                @Override
                public void onChangeStar(int rate) {
                }
            });
            ProxRateDialog.init(getContext(),config);
            ProxRateDialog.showAlways(getChildFragmentManager());
        }else{
            // Khôg làm gì cả
        }


        // Mark each screen
        App.getInstance().setMarkEachScreen(1);

        mModel.setContext(getContext());

        tvAllNotes = findViewById(R.id.tv_all_notes_menu);
        tvPinned = findViewById(R.id.tv_sum_pinned_menu);

        findViewById(R.id.constraint_layout_all_notes_menu,this);
        findViewById(R.id.constraint_layout_pinned_menu,this);
        findViewById(R.id.constraint_layout_donate_menu,this);
        showNotesPinned();

    }

    private void showNotesPinned() {
        tvAllNotes.setText(mModel.getListUserModel().size()+"");
        tvPinned.setText(mModel.getListUserModelPinned("false").size()+"");
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frag_menu;
    }

    @Override
    public void onClick(View v) {
        Bundle params = new Bundle();
        if(v.getId()==R.id.constraint_layout_all_notes_menu){
            //Check event
            params.putString("event_type","click_AllNotes");
            mFirebaseAnalytics.logEvent("Home_layout",params);

            // Nếu click vào all_notes_menu sẽ hiện cả pinned và notes thì gán bằng true
            App.getInstance().setCheckPinnedorAllNotesMenu(true);
            callBack.onCallback(KEY_HOME_FRAGMENT1,null);

        }else if(v.getId()==R.id.constraint_layout_pinned_menu){
            //Check event
            params.putString("event_type","click_Pinned");
            mFirebaseAnalytics.logEvent("Home_layout",params);

            // Nếu click vào pinned_menu sẽ hiện cả pinned và notes thì gán bằng true
            App.getInstance().setCheckPinnedorAllNotesMenu(false);
            callBack.onCallback(KEY_HOME_FRAGMENT_NOT_NOTES,null);
        }else if(v.getId()==R.id.constraint_layout_donate_menu){
            //Check event
            params.putString("event_type","click_Donate");
            mFirebaseAnalytics.logEvent("Home_layout",params);
            callBack.onCallback(KEY_DONATE_FRAGMENT,null);
        }

    }
}
