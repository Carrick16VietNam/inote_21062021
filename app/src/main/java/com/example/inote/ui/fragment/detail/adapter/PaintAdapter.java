package com.example.inote.ui.fragment.detail.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.inote.R;
import com.example.inote.datasource.model.PaintModel;
import com.example.inote.ui.callback.OnActionCallBack;

import java.util.List;

public class PaintAdapter extends RecyclerView.Adapter<PaintAdapter.ViewHolder> {
    public static final String KEY_DELETE_PAINT_DETAIL = "KEY_DELETE_PAINT_DETAIL";
    public static final String KEY_SHOW_PAINT_DETAIL = "KEY_SHOW_PAINT_DETAIL";
    private Context context;
    private List<PaintModel> mListPaint;
    private OnActionCallBack callBack;

    public PaintAdapter(Context context, List<PaintModel> mListPaint) {
        this.context = context;
        this.mListPaint = mListPaint;
    }

    public void setNewData(List<PaintModel> list){
        mListPaint = list;
        notifyDataSetChanged();
    }

    public void setCallBack(OnActionCallBack callBack) {
        this.callBack = callBack;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_item_paint_detail,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PaintModel paintModel = mListPaint.get(position);
//        holder.imvPaintDetail.setImageURI(Uri.parse(paintModel.getPath()));
        Glide.with(context).load(paintModel.getPath()).into(holder.imvPaintDetail);

        holder.imvDeletePaintDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.onCallBackPaint(KEY_DELETE_PAINT_DETAIL,paintModel);
            }
        });

        holder.imvPaintDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.onCallBackPaint(KEY_SHOW_PAINT_DETAIL,paintModel);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mListPaint.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imvPaintDetail , imvDeletePaintDetail;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imvPaintDetail = itemView.findViewById(R.id.imv_paint_detail);
            imvDeletePaintDetail = itemView.findViewById(R.id.imv_delete_paint_detail);

        }
    }
}
