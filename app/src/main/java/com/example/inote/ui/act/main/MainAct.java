package com.example.inote.ui.act.main;


import android.content.SharedPreferences;
import android.os.Bundle;
import com.example.inote.R;
import com.example.inote.base.BaseActivity;
import com.example.inote.datasource.model.UserModel;
import com.example.inote.ui.callback.OnActionCallBackFinishApp;
import com.example.inote.ui.callback.OnActionCallbackFragment;
import com.example.inote.ui.fragment.detail.DetailFragment;
import com.example.inote.ui.fragment.detail.Draw.DrawFragment;
import com.example.inote.ui.fragment.donate.DonateFragment;
import com.example.inote.ui.fragment.home.HomeFragment;
import com.example.inote.ui.fragment.menu.MenuFragment;
import com.example.inote.utils.admanager.AdManager;
import com.example.inote.utils.app.App;
import com.google.android.gms.ads.LoadAdError;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.xuandq.rate.ProxRateDialog;
import com.xuandq.rate.RatingDialogListener;

public class MainAct extends BaseActivity<MainViewModel> implements OnActionCallbackFragment, OnActionCallBackFinishApp {
    public static final String KEYBUNDLE_PINNEDADAPTER_DETAIL = "KEYBUNDLE_PINNEDADAPTER_DETAIL";
    public static final String KEYBUNDLE_HOMEFRAGMENT_DETAILFRAGMENT = "KEYBUNDLE_HOMEFRAGMENT_DETAILFRAGMENT";
    public static final String KEYBUNDLE_NOTESADAPTER_DETAIL = "KEYBUNDLE_NOTESADAPTER_DETAIL";
    public static final String KEY_USER_DIALOG_FRAGMENT = "KEY_USER_DIALOG_FRAGMENT";
    public static final String KEY_COUNT_START = "KEY_COUNT_START";
    public static final String KEY_BOOLEAN = "KEY_BOOLEAN";
    public static final String RATE_ONE_TURN = "RATE_ONE_TURN";
    public static final String KEY_RATE = "KEY_RATE";
    public static String KEY_BUNDLE = "";
    private HomeFragment homeFragment;
    private MenuFragment menuFragment;
    private DetailFragment detailFragment;
    private DetailFragment detailFragmentFromPinnedAdapter;
    private DetailFragment detailFragmentFromNotesAdapter;
    private DrawFragment drawFragment;
    private DonateFragment donateFragment;
    private SharedPreferences sharedPreferences , sharedPreferences1;
    public static SharedPreferences sharedPreferencesRateOneTurn;
    public static SharedPreferences.Editor myEdit2;
    private SharedPreferences.Editor myEdit1;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        // Khi mà vào lần 2 sẽ hiện push rate
        sharedPreferences = getSharedPreferences("MySharedPref",MODE_PRIVATE);
        SharedPreferences.Editor myEdit = sharedPreferences.edit();
        int i = sharedPreferences.getInt(KEY_COUNT_START,0);
        i = i+1;
        myEdit.putInt(KEY_COUNT_START,i);
        myEdit.apply();

        // check khi bakpress
        sharedPreferences1 = getSharedPreferences("ON_PRESSED",MODE_PRIVATE);
        myEdit1 = sharedPreferences1.edit();

        // khoi tao push rate chỉ hiện 1 lần
        sharedPreferencesRateOneTurn = getSharedPreferences(RATE_ONE_TURN,MODE_PRIVATE);
        myEdit2 = sharedPreferencesRateOneTurn.edit();


    }


    @Override
    protected Class getClassViewModel() {
        return MainViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initViews() {
        showHomeFragment();
    }

    @Override
    public void onBackPressed() {
        if(App.getInstance().getMarkEachScreen()==1){
            if(sharedPreferences1.getBoolean(KEY_BOOLEAN,false)==false&&sharedPreferencesRateOneTurn.getBoolean(KEY_RATE,false)==false){
                ProxRateDialog.Config config = new ProxRateDialog.Config();
                config.setListener(new RatingDialogListener() {
                    @Override
                    public void onSubmitButtonClicked(int rate, String comment) {
                        Bundle params = new Bundle();
                        params.putString("event_type","rated");
                        params.putString("comment",comment);
                        params.putInt("star",rate);
                        mFirebaseAnalytics.logEvent("prox_rating_layout",params);

                        myEdit1.putBoolean(KEY_BOOLEAN,true);
                        myEdit1.apply();

                        myEdit2.putBoolean(KEY_RATE,true);
                        myEdit2.apply();

                        finishAndRemoveTask();

                    }
                    @Override
                    public void onLaterButtonClicked() {
                    }

                    @Override
                    public void onChangeStar(int rate) {
                        if(rate>=4){
                            finishAndRemoveTask();
                        }
                    }
                });
                ProxRateDialog.init(this,config);
                ProxRateDialog.showAlways(getSupportFragmentManager());
            }else{
                super.onBackPressed();
            }

        }else{
            super.onBackPressed();
        }

    }



    @Override
    public void onCallback(String key, Object object) {
        switch (key){
            case HomeFragment.KEY_MENU_FRAGMENT:
                showMenuFragment();
                break;
            case MenuFragment.KEY_HOME_FRAGMENT1:
                showHomeFragment();
                break;
            case HomeFragment.KEY_DETAIL_FRAGMENT:
                showDetailFragment(object);
                break;
            case HomeFragment.KEY_HOMEFRAGMENT_TO_MAIN_TO_DETAILFRAGMENT_PIN:
                transmissionDataPinAdapterToDetail(object);
                break;
            case HomeFragment.KEY_HOMEFRAGMENT_TO_MAIN_TO_DETAILFRAGMENT_NOTES:
                transmissionDataNotesAdapterToDetail(object);
                break;
            case DetailFragment.KEY_DRAW_PAINT_FRAGMENT:
                drawFragment = new DrawFragment();
                UserModel userModel = (UserModel) object;
                Bundle bundle = new Bundle();
                bundle.putSerializable(KEY_USER_DIALOG_FRAGMENT,userModel);
                drawFragment.setArguments(bundle);
                showFragment(R.id.frame_layout_main, drawFragment,true);
                break;
            case MenuFragment.KEY_HOME_FRAGMENT_NOT_NOTES:
                showHomeFragmentNotNotesWhenClickPinnedMenu();
                break;
            case MenuFragment.KEY_DONATE_FRAGMENT:
                showDonateFragment();
        }
    }

    private void showDonateFragment() {
        DonateFragment donateFragment = new DonateFragment();
        showFragment(R.id.frame_layout_main,donateFragment,true);
    }

    private void showHomeFragmentNotNotesWhenClickPinnedMenu() {
        showHomeFragment();
    }


    private void transmissionDataNotesAdapterToDetail(Object object) {
        detailFragmentFromNotesAdapter = new DetailFragment();
        detailFragmentFromNotesAdapter.setCallback(this);
        UserModel userModel = (UserModel) object;
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEYBUNDLE_NOTESADAPTER_DETAIL,userModel);
        detailFragmentFromNotesAdapter.setArguments(bundle);
        KEY_BUNDLE = "KEYBUNDLE_NOTESADAPTER_DETAIL";
        showFragment(R.id.frame_layout_main,detailFragmentFromNotesAdapter,true);
    }

    private void transmissionDataPinAdapterToDetail(Object object) {
        detailFragmentFromPinnedAdapter = new DetailFragment();
        detailFragmentFromPinnedAdapter.setCallback(this);
        UserModel userModel = (UserModel) object;
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEYBUNDLE_PINNEDADAPTER_DETAIL,userModel);
        detailFragmentFromPinnedAdapter.setArguments(bundle);
        KEY_BUNDLE = "KEYBUNDLE_PINNEDADAPTER_DETAIL";
        showFragment(R.id.frame_layout_main,detailFragmentFromPinnedAdapter,true);
    }

    private void showDetailFragment(Object object) {
        detailFragment = new DetailFragment();
        detailFragment.setCallback(this);
        UserModel userModel = (UserModel) object;
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEYBUNDLE_HOMEFRAGMENT_DETAILFRAGMENT,userModel);
        detailFragment.setArguments(bundle);
        KEY_BUNDLE = "KEYBUNDLE_HOMEFRAGMENT_DETAILFRAGMENT";
        showFragment(R.id.frame_layout_main,detailFragment,true);
    }

    private void showMenuFragment() {
        menuFragment = new MenuFragment();
        menuFragment.setCallBack(this);
        menuFragment.setCallBackFinishApp(this);
        showFragment(R.id.frame_layout_main,menuFragment,false);
    }

    private void showHomeFragment() {
        homeFragment = new HomeFragment();
        homeFragment.setCallback(this);
        showFragment(R.id.frame_layout_main,homeFragment,false);
    }

    @Override
    public void finishApp() {
        finish();
    }
}