package com.example.inote.ui.fragment.home;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;

import com.example.inote.base.BaseViewModel;
import com.example.inote.datasource.database.UserDao;
import com.example.inote.datasource.database.UserDatabase;
import com.example.inote.datasource.model.PictureModel;
import com.example.inote.datasource.model.UserModel;

import java.util.ArrayList;
import java.util.List;

public class HomeViewModel extends BaseViewModel {
    private List<UserModel> listUserModel;
    private UserDao userDao;
    private Context context;

    // thừa nhưng mới đầu viết ngu vđ
    public HomeViewModel() {
        listUserModel = new ArrayList<>();
    }

    // thừa nhưng mới đầu viết ngu vđ
    public List<UserModel> getListUserModel() {
        listUserModel = userDao.listUser();
        return listUserModel;
    }

    // Các phương thức để truy vấn vào database
    public void insertUser(UserModel userModel) {
        userDao.insertUser(userModel);
    }

    public void deleteUser(UserModel userModel){
        userDao.deleteUser(userModel);
    }

    public void updateUser(UserModel userModel){
        userDao.updateUser(userModel);
    }

    public List<UserModel> sortAZ(String str){
        return userDao.listUserAZ(str);
    }

    public List<UserModel> sortZA(String str){
        return userDao.listUserZA(str);
    }

    public List<UserModel> searchUserModel(String str , String check){
        return userDao.searchUser(str,check);
    }

    public List<UserModel> getListPinnedOrNotes(String str){
        return userDao.listPinnedOrNotes(str);
    }

    public List<PictureModel> getListPictureOfId(int id){
        return userDao.listPictureModel(id);
    }

    // Phải có context để khởi tạo userDao trong ViewModel được
    public void setContext(Context context) {
        this.context = context;
        userDao = UserDatabase.getInstance(context).userDao();
    }
}
