package com.example.inote.ui.act.splash;

import android.content.Intent;
import com.example.inote.R;
import com.example.inote.base.BaseActivity;
import com.example.inote.ui.act.main.MainAct;
import com.example.inote.utils.admanager.AdManager;


public class SplashAct extends BaseActivity<SplashViewModel> {
    @Override
    protected Class<SplashViewModel> getClassViewModel() {
        return SplashViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frag_splash;
    }

    @Override
    protected void initViews() {
        goToMainAct();
    }

    private void goToMainAct() {
        Intent intent = new Intent(SplashAct.this, MainAct.class);
        startActivity(intent);
        finish();
    }
}
