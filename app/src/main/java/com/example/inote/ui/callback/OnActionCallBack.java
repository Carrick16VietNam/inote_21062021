package com.example.inote.ui.callback;

import com.example.inote.datasource.model.CheckboxModel;
import com.example.inote.datasource.model.PaintModel;
import com.example.inote.datasource.model.PictureModel;

public interface OnActionCallBack {
    void onCallBackCheckbox(String key , CheckboxModel checkboxModel);
    void onCallBackPicture(String key , PictureModel pictureModel);
    void onCallBackPaint(String key , PaintModel paintModel);
}
